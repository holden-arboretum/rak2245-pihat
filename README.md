# RAK2245-PiHat
- The RAK2245-PiHat is the newest LoRa Module that has been selected for this board. It has replaced the sx1301a that was being used previously because that board is no longer commercially available. This new module mounts directly onto the 40 pin bus of a Raspberry Pi and is much easier to get the gateway working.
- If a new raspberry pi is purchased and the gateway needs to be reconnected with The Things Network, follow these [instructions](https://www.thethingsnetwork.org/docs/gateways/rak2245/device-firmware-setup.html)
- Some notes for configuring the gateway:
    - When configuring the LoRa concentrator, select TTN as the LoRa server and choose the US_902_928 plan
    - When configuring the WiFi, you want to be in mode 2: Client Mode. This will allow you to configure a wireless connection
        - The tricky part about the CaseGuest WiFi is that it does not require a password, yet the program requires you to input a password to continue. Just put a dummy password in temporarily and continue on. The password will have to be changed manually in the wpa-supplicant file.
        - Once the Pi reboots, login and enter the commands

        ```bash
        cd /etc/wpa-supplicant
        ```
        ```bash
        sudo cat wpa-supplicant.conf
        ```
        - This will open up the file that stores the WiFi name and password
        - Delete the password line and replace it with
        
        ```bash
        key_mgmt=NONE
        ```
        - Press Ctrl + X and press Y then ENTER to save the file
        - Reboot the Pi and the WiFi should be working
        - Continue on with the tutorial linked above to register the gateway on TTN

